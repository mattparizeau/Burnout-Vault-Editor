﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using BurnoutVaultEditor.Exceptions;

namespace BurnoutVaultEditor
{
    public partial class MainForm : Form
    {
        private const string WindowTitle = "Burnout Vault Editor";

        private VaultResource CurrentResource;
        private string CurrentPath;
        
        public bool IsDirty
        {
            get
            {
                if (CurrentResource == null)
                    return false;
                return CurrentResource.IsDirty;
            }
            set
            {
                if (CurrentResource == null)
                    return;
                bool oldDirty = CurrentResource.IsDirty;
                CurrentResource.IsDirty = value;
                if (CurrentResource.IsDirty != oldDirty)
                    UpdateDisplay();
            }
        }

        public MainForm()
        {
            InitializeComponent();

            UpdateDisplay();
        }

        private delegate void UpdateDisplayDelegate();
        private void UpdateDisplay()
        {
            if (InvokeRequired)
            {
                Invoke(new UpdateDisplayDelegate(UpdateDisplay));
            }
            else
            {
                if (CurrentResource == null)
                {
                    Text = WindowTitle;

                    tsbSave.Enabled = false;
                    tsbSaveAs.Enabled = false;
                    saveToolStripMenuItem.Enabled = false;
                    saveAsToolStripMenuItem.Enabled = false;
                }
                else
                {
                    string path = string.IsNullOrEmpty(CurrentPath) ? "Untitled" : Path.GetFileName(CurrentPath);
                    Text = (IsDirty ? "*" : "") + path + " - " + WindowTitle;

                    tsbSave.Enabled = IsDirty;
                    tsbSaveAs.Enabled = true;
                    saveToolStripMenuItem.Enabled = IsDirty;
                    saveAsToolStripMenuItem.Enabled = true;
                }

                dbgMain.SelectedObject = CurrentResource;
            }
        }

        private bool CheckContinue()
        {
            if (IsDirty)
            {
                string path = string.IsNullOrEmpty(CurrentPath) ? "Untitled" : CurrentPath;
                DialogResult result = MessageBox.Show(this, "Save changes to \"" + path + "\"?", "Save changes", MessageBoxButtons.YesNoCancel);
                if (result == DialogResult.Yes)
                    return Save();
                else if (result == DialogResult.No)
                    return true;
                else
                    return false;
            }
            else
            {
                return true;
            }
        }

        private void Open()
        {

            if (!CheckContinue())
                return;

            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Burnout Vault File|*.bin;*.bundle;*.bndl|All Files|*.*";
            DialogResult result = ofd.ShowDialog(this);
            if (result == DialogResult.OK)
                DoOpen(ofd.FileName);
        }

        public delegate Loader GetLoaderDelagate();
        public static Loader loader;

        public Loader GetLoader()
        {
            return loader;
        }

        public void DoOpen(string path)
        {
            loader = new Loader();
            CancellationToken token = new CancellationToken(false);
            loader.Cancelled += () =>
            {
                token.ThrowIfCancellationRequested();
            };
            loader.SetTask("Loading " + System.IO.Path.GetFileName(path));

            Task.Run(() =>
            {
                Loader loader2 = (Loader)Program.Invoker.Invoke(new GetLoaderDelagate(GetLoader));
                try
                {
                    try
                    {
                        CurrentResource = VaultResource.Read(loader2, path);

                        CurrentPath = path;
                        IsDirty = false;
                        UpdateDisplay();
                        loader.Finish();
                    }
                    catch (ReadFailedError ex)
                    {
                        CurrentPath = "";
                        IsDirty = false;
                        UpdateDisplay();
                        loader.Finish();

                        MessageBox.Show(ex.Message, "Load Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (OperationCanceledException ex)
                {
                    CurrentResource = null;

                    CurrentPath = "";
                    IsDirty = false;

                    UpdateDisplay();

                    loader.Finish();
                }
            }, token);

            loader.ShowDialog(this);
        }

        private bool Save()
        {
            if (CurrentResource == null)
                return false;

            if (string.IsNullOrEmpty(CurrentPath))
                return SaveAs();
            return DoSave(CurrentPath);
        }

        private bool SaveAs()
        {
            if (CurrentResource == null)
                return false;

            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Burnout Vault File|*.bin;*.bundle;*.bndl|All Files|*.*";
            DialogResult result = sfd.ShowDialog(this);
            if (result == DialogResult.OK)
            {
                return DoSave(sfd.FileName);
            }
            return false;
        }

        private bool DoSave(string path)
        {
            loader = new Loader();
            CancellationToken token = new CancellationToken(false);
            loader.Cancelled += () =>
            {
                token.ThrowIfCancellationRequested();
            };
            loader.SetTask("Saving " + System.IO.Path.GetFileName(path));

            Task.Run(() =>
            {
                Loader loader2 = (Loader)Program.Invoker.Invoke(new GetLoaderDelagate(GetLoader));
                try
                {
                    CurrentResource.Write(loader2, path);

                    CurrentPath = path;
                    IsDirty = false;

                    loader.Finish();
                }
                catch (OperationCanceledException ex)
                {
                    CurrentPath = path;
                    IsDirty = true;

                    loader.Finish();
                }
            }, token);

            loader.ShowDialog(this);

            return true;
        }

        private void Exit()
        {
            Application.Exit();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveAs();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Exit();
        }

        private void tsbOpen_Click(object sender, EventArgs e)
        {
            Open();
        }

        private void tsbSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void tsbSaveAs_Click(object sender, EventArgs e)
        {
            SaveAs();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!CheckContinue())
                e.Cancel = true;
        }
    }
}
